<?php

namespace App\Http\Controllers;
use App\Models\Relative;
use Illuminate\Http\Request;
use App\Models\User;
class RelativesController extends Controller
{
    
    public function index()
    {
        $relatives = Relative::all();

        return response()->json($relatives);
    }

    public function store(Request $request)
    {
        $relative = relative::create($request->all());

        return response()->json($relative, 201);
    }

    public function show($id)
    {
        $relative = Relative::find($id);

        return response()->json($relative, 200);
     }

     public function update(Request $request, $id)
     {
         $relative = Relative::find($id);

         $relative->update($request->all());
 
         return response()->json($relative, 200);
     }

     public function delete(Relative $id )
     {
         $id->delete();
 
         return response()->json(null, 204);
     }
}
