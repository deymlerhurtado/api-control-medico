<?php

namespace App\Http\Controllers;
use App\Models\Diagnostico;
use Illuminate\Http\Request;
use App\Models\User;
class DiagnosticosController extends Controller
{
    public function index()
    {
        $diagnostico = Diagnostico::all();

        return response()->json($diagnostico);
    }

    public function store(Request $request)
    {
        $diagnostico = Diagnostico::create($request->all());

        return response()->json($diagnostico, 201);
    }

    public function show($id)
    {
        $diagnostico = Diagnostico::find($id);

        return response()->json($diagnostico, 200);
     }

     public function update(Request $request, $id)
     {
         $diagnostico = Diagnostico::find($id);

         $diagnostico->update($request->all());
 
         return response()->json($diagnostico, 200);
     }

     public function delete(Diagnostico $id )
     {
         $id->delete();
 
         return response()->json(null, 204);
     }
}
