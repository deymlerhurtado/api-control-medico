<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diagnostico extends Model
{
    use HasFactory;
    protected $fillable = [
        'paciente',
        'doctor',
        'descripcion',
        'fecha_diagnostico'
     
    ];
}
