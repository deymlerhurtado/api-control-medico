<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relative extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombre',
        'apellido',
        'dpi',
        'direccion',
        'telefono',
        'fecha_nacimiento'
    ];
}
