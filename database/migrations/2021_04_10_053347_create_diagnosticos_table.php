<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiagnosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     /*   id
paciente
doctor
descripcion
fecha_diagnostico*/
        Schema::create('diagnosticos', function (Blueprint $table) {
            $table->id();
            $table->string('paciente');
            $table->string('doctor');
            $table->string('descripcion');
            $table->date('fecha_diagnostico');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnosticos');
    }
}
