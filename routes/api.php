<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\RelativesController;
use App\Http\Controllers\DiagnosticosController;

Route::post('/register', [AuthController::class, 'register']);

Route::post('/login', [AuthController::class, 'login']);

//Route::post('/me', [AuthController::class, 'me']);
Route::post('/me', [AuthController::class, 'me'])->middleware('auth:sanctum');
//rutas relatives->familiares
Route::post('/relatives', [RelativesController::class, 'store'])->middleware('auth:sanctum');
Route::get('/relatives', [RelativesController::class, 'index'])->middleware('auth:sanctum');
Route::get('/relatives/{id}', [RelativesController::class, 'show'])->middleware('auth:sanctum');
Route::put('/relatives/{id}', [RelativesController::class, 'update'])->middleware('auth:sanctum');
Route::delete('/relatives/{id}', [RelativesController::class, 'delete'])->middleware('auth:sanctum');
//rutas diagnostico
Route::post('/diagnosticos', [DiagnosticosController::class, 'store'])->middleware('auth:sanctum');
Route::get('/diagnosticos', [DiagnosticosController::class, 'index'])->middleware('auth:sanctum');
Route::get('/diagnosticos/{id}', [DiagnosticosController::class, 'show'])->middleware('auth:sanctum');
Route::put('/diagnosticos/{id}', [DiagnosticosController::class, 'update'])->middleware('auth:sanctum');
Route::delete('/diagnosticos/{id}', [DiagnosticosController::class, 'delete'])->middleware('auth:sanctum');
